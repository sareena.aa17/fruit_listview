
import 'package:flutter/material.dart';
import 'package:fruit_listview/pages/fruit_detail.dart';

import '../models/fruit_data_model.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static List<String> fruitname = [
    'Apple', 'Banana', 'Mango', 'Orange', 'Pineapple'
  ];
  static List url = [
    'https://source.unsplash.com/rxN2MRdFJVg',
    'https://cdn.mos.cms.futurecdn.net/42E9as7NaTaAi4A6JcuFwG-1200-80.jpg',
    'https://media.istockphoto.com/photos/mango-isolated-on-white-background-picture-id911274308?k=20&m=911274308&s=612x612&w=0&h=YY8-xqycxsqFea5B-JdhlcgExlXYWMiFoLJdQ-LUx5E=',
    'https://5.imimg.com/data5/VN/YP/MY-33296037/orange-600x600-500x500.jpg',
    'https://5.imimg.com/data5/GJ/MD/MY-35442270/fresh-pineapple-500x500.jpg'
  ];

  static List description = [
    'Apple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Banana is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Mango is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Orange is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Pineapple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
  ];

  final List<FruitDataModel> fruitdata = List.generate(fruitname.length,
          (index) => FruitDataModel('${fruitname[index]}', '${url[index]}', '${description[index]}'));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: fruitdata.length,
          itemBuilder: (context, index){
            return Container(
              height: 100,
              child: Card(
                child: Align(
                child: ListTile(
                leading: SizedBox(
                  width: 70,
                  height: 70,
                child: Image.network(fruitdata[index].ImageUrl),
            ),
                  title: Text(fruitdata[index].name),
                  onTap: (){
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => FruitDetail(
                          fruitDataModel: fruitdata[index],
                        )));
                  },
                ),
              ),
              ),
            );
          }
      ),
    );
  }
}
